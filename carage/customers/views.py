#from django.shortcuts import render
import datetime
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import CreateView, DeleteView#, ListView, DetailView, UpdateView
#from django.http import HttpResponse
from django.conf import settings
from django.views.generic.base import TemplateView
from django.core.mail import EmailMessage#, message
from django.http.response import HttpResponseRedirect #HttpResponse
from django.contrib import messages
from django.template.loader import get_template
from django.views.generic.detail import DetailView
from django.views.generic.edit import UpdateView
from django.urls import reverse_lazy
from django_tables2 import SingleTableView
from .tables import AppointmentTable
from .models import Appointment
from .forms import AppointmentForm
#from django.template import Context
# Create your views here.


class HomeTemplateView(TemplateView):
    template_name = "index.html"

    def post(self, request):
        name = request.POST.get("name") 
        email = request.POST.get("email")
        message = request.POST.get("message")

        email = EmailMessage(
            subject= f"{name} from CarAge front page.",
            body=message,
            from_email=settings.EMAIL_HOST_USER,
            to=[settings.EMAIL_HOST_USER], 
            reply_to=[email]
        )
        email.send()
        messages.add_message(request, messages.INFO, f"Thanks {name} for your email!")

        return HttpResponseRedirect(request.path)


class AppointmenCreateView(CreateView):
    form_class = AppointmentForm
    #fields = ["first_name","last_name", "email", "phone","request"]
    template_name = "appointment.html"


    def post(self, request, *args, **kwargs):
        form = self.get_form()
        print("Appointment post")
        if form.is_valid():
            obj = form.save(commit=False)
            obj.save()
            messages.add_message(request, messages.SUCCESS, f"Thanks {obj.first_name} for requesting a time, we will email you ASAP!")
        return HttpResponseRedirect(request.path)


# class ManageAppointmentTemplateView(LoginRequiredMixin,ListView):
#     template_name = "manage-appointments.html"
#     model = Appointment
#     context_object_name = "appointments"
#     login_required = True
#     paginate_by = 3


#     def post(self, request):
#         date = request.POST.get("date")
#         appointment_id = request.POST.get("appointment-id")
#         appointment = Appointment.objects.get(id=appointment_id)
#         appointment.accepted = True
#         appointment.accepted_date =  datetime.datetime.now()
#         appointment.appointment_date = date
#         appointment.save()


#         data = {
#             "fname":appointment.first_name,
#             "date":date,
#         }

#         message = get_template('email.html').render(data)
#         email = EmailMessage(
#             "About your time request",
#             message,
#             settings.EMAIL_HOST_USER,
#             [appointment.email],
#         )
#         email.content_subtype = "html"
#         email.send()

#         messages.add_message(request, messages.SUCCESS, f"You accepted the appointment of {appointment.first_name}")
#         return HttpResponseRedirect(request.path)

    # def get_context_data(self,*args, **kwargs):
    #     context = super().get_context_data(*args, **kwargs)
    #     #appointments = Appointment.objects.all()
    #     context.update({   
    #         "title":"Manage times"
    #     })
    #     return context

# class AppointmentListViewOLD(LoginRequiredMixin, ListView):
#     template_name = "appointment_list.html"
#     model=Appointment
#     context_object_name = "appointments"
#     paginate_by = 4

#     def post(self, request):
#         date = request.POST.get("date")
#         appointment_id = request.POST.get("appointment-id")
#         appointment = Appointment.objects.get(id=appointment_id)
#         appointment.accepted = True
#         appointment.accepted_date = datetime.datetime.now()
#         appointment.appointment_date = date
#         appointment.save()

#         data = {
#             "fname":appointment.first_name,
#             "date":date,
#         }

#         message = get_template('email.html').render(data)
#         email = EmailMessage(
#             "About your time request",
#             message,
#             settings.EMAIL_HOST_USER,
#             [appointment.email],
#         )
#         email.content_subtype = "html"
#         email.send()

#         messages.add_message(request, messages.SUCCESS, f"You accepted the appointment of {appointment.first_name}")
#         return HttpResponseRedirect(request.path)

class AppointmentListView(LoginRequiredMixin, SingleTableView):
    '''View for appointment management. The queryset excludes invoiced
    appointments. They should not and cannot be deleted.
    '''
    template_name = "appointment_list.html"
    model=Appointment
    table_class = AppointmentTable
    context_object_name = "appointments"
    queryset = Appointment.objects.exclude(invoices__isnull=False).distinct()
    paginate_by = 3


    def post(self, request):
        date = request.POST.get("date")
        if date=="":
            messages.add_message(request, messages.ERROR, f"Cannot accept without an appointment date!")
            return HttpResponseRedirect(request.path)
        else:
            appointment_id = request.POST.get("appointment-id")
            appointment = Appointment.objects.get(id=appointment_id)
            appointment.accepted = True
            appointment.accepted_date = datetime.datetime.now()
            appointment.appointment_date = date
            appointment.save()

            data = {
               "fname":appointment.first_name,
               "date":date,
            }

            message = get_template('email.html').render(data)
            email = EmailMessage(
                "About your time request",
              message,
              settings.EMAIL_HOST_USER,
              [appointment.email],
         )
            email.content_subtype = "html"
            email.send()
            messages.add_message(request, messages.SUCCESS, f"You accepted the appointment of {appointment.first_name}. Email was sent.")
            return HttpResponseRedirect(request.path)

class DeleteAppointmentView(LoginRequiredMixin, DeleteView):# This is quite risky solution at the moment
    template_name = "confirm_delete.html" #could be empty html now? check later
    model=Appointment
    success_url =reverse_lazy("appointmentlist")
    def get(self, request, *args, **kwargs):
        return self.post(request, *args, **kwargs)




class InvoiceView(LoginRequiredMixin, TemplateView):#LoginRequiredMixin
    '''The basic routing view for accessing the VueSPA.'''
    template_name = "customers/invoice.html"

# These are not used yet
class AppointmentDetailView(LoginRequiredMixin, DetailView):
    '''Not in use. For later functionality'''
    #template_name = "manage-appointments_copy.html"
    model=Appointment

class AppointmentUpdateView(LoginRequiredMixin, UpdateView):
    '''Not in use. For later functionality'''
    #template_name = "manage-appointments_copy.html"
    model=Appointment

class AppointmentCreateView(LoginRequiredMixin, CreateView):
    '''Not in use. For later functionality'''
    model = Appointment
    #form_class = AppointmentForm


from django_tables2 import tables, TemplateColumn
from carage.customers.models import Appointment

class AppointmentTable(tables.Table):
    class Meta:
        model = Appointment
        attrs = {'class': 'table table-sm'}
        fields = ['first_name','last_name', 'email', 'phone','appointment_date'] 

    appointment_date = TemplateColumn(template_name='buttoncolumn.html')

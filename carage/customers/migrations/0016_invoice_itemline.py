# Generated by Django 3.1.1 on 2021-09-23 12:19

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('customers', '0015_auto_20210923_1206'),
    ]

    operations = [
        migrations.CreateModel(
            name='Invoice',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date', models.DateField()),
                ('due_date', models.DateField()),
                ('state', models.CharField(choices=[('PAID', 'Paid'), ('UNPAID', 'Unpaid'), ('CANCELLED', 'Cancelled')], default='UNPAID', max_length=15)),
                ('appointment', models.ForeignKey(blank=True, default=None, null=True, on_delete=django.db.models.deletion.PROTECT, related_name='invoices', to='customers.appointment')),
            ],
        ),
        migrations.CreateModel(
            name='ItemLine',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('quantity', models.IntegerField()),
                ('description', models.CharField(max_length=500)),
                ('price', models.DecimalField(decimal_places=2, max_digits=8)),
                ('taxed', models.BooleanField()),
                ('invoice', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='items', to='customers.invoice')),
            ],
        ),
    ]

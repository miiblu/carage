# Generated by Django 3.1.1 on 2021-09-29 13:04

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('customers', '0020_auto_20210927_1053'),
    ]

    operations = [
        migrations.AlterField(
            model_name='appointment',
            name='registered_customer',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, related_name='appointments', to='customers.customer'),
        ),
    ]

# Generated by Django 3.1.1 on 2021-09-29 14:17

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('customers', '0022_remove_appointment_registered_customer'),
    ]

    operations = [
        migrations.DeleteModel(
            name='Customer',
        ),
    ]

# Generated by Django 3.1.1 on 2021-09-05 18:28

import autoslug.fields
import carage.customers.models
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('customers', '0004_auto_20210905_0443'),
    ]

    operations = [
        migrations.RenameField(
            model_name='namemaybe',
            old_name='name',
            new_name='firstname',
        ),
        migrations.RemoveField(
            model_name='namemaybe',
            name='surname',
        ),
        migrations.AddField(
            model_name='namemaybe',
            name='lastname',
            field=models.CharField(default='', max_length=255, verbose_name='Last name'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='namemaybe',
            name='slug',
            field=autoslug.fields.AutoSlugField(default='', editable=False, populate_from=['firstname', 'lastname'], slugify=carage.customers.models.namesSlugify, unique=True, verbose_name='Names slug'),
        ),
    ]

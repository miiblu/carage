# Generated by Django 3.1.1 on 2021-09-14 06:42

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('customers', '0013_auto_20210911_1413'),
    ]

    operations = [
        migrations.DeleteModel(
            name='Customer',
        ),
    ]

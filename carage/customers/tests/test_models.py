import pytest
#import factory
#import factory.fuzzy
#from datetime import datetime, timezone#timedelta, 

#from carage.customers.models import Appointment
from .factories import AppointmentFactory

pytestmark = pytest.mark.django_db



def test_appointment__str__():
    tested = AppointmentFactory()
    assert tested.__str__() == str(tested.first_name)+ " " + str(tested.last_name)
    assert str(tested) == tested.__str__()
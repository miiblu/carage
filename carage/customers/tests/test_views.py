import pytest

from pytest_django.asserts import (
assertContains,
#assertRedirects
)
from django.urls import reverse
#from django.contrib.sessions.middleware import SessionMiddleware
from django.test import TestCase
#from django.test import Client
from django.core import mail
#from django.test import RequestFactory
from carage.users.models import User
#from ..models import Appointment
from ..views import (
HomeTemplateView,
AppointmenCreateView,
#AppointmentListView,  # later, might change very soon (14.9.)
#DeleteAppointmentView,

)
from .factories import AppointmentFactory
pytestmark = pytest.mark.django_db

def test_HomeTemplateView(rf):
    request = rf.get(reverse("home"))
    response = HomeTemplateView.as_view()(request)
    assertContains(response, 'Our CarAge experts')

def test_AppointmenCreateView(rf):
    request = rf.get(reverse("appointment"))
    response = AppointmenCreateView.as_view()(request)
    assertContains(response, 'Get a time')

class AppointmentContactTests(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.appointment = AppointmentFactory.create()
    def test_view_url_exists_at_desired_location(self):
        response = self.client.get('/make-an-appointment/')
        self.assertEqual(response.status_code, 200)
    def test_view_url_accessible_by_name(self):
        response = self.client.get(reverse('appointment'))
        self.assertEqual(response.status_code, 200)
    def test_view_uses_correct_template(self):
        response = self.client.get(reverse('appointment'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'appointment.html')
    def test_appointment_post_redirects_and_gives_message(self):
        response = self.client.post(reverse('appointment'),  {'first_name' : self.appointment.first_name,
        'last_name': self.appointment.last_name, 
        'email': self.appointment.email, 
        'phone': self.appointment.phone,  
        'request': self.appointment.request}, follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertRedirects(response, reverse('appointment'))
        messages = list(response.context['messages'])
        self.assertEqual(len(messages), 1)
        self.assertEqual(str(messages[0]), "Thanks " + self.appointment.first_name + " for requesting a time, we will email you ASAP!")
        self.assertNotContains(response, 'something went wrong' ,200)



class HomeContactTests(TestCase):
    def test_view_url_exists_at_desired_location(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)
    def test_view_url_accessible_by_name(self):
        response = self.client.get(reverse('home'))
        self.assertEqual(response.status_code, 200)
    def test_view_uses_correct_template(self):
        response = self.client.get(reverse('home'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'index.html')
    def test_contact_post_redirects(self): 
        appointment = AppointmentFactory() # Close enough.
        response = self.client.post(reverse('home'),  {'name' : appointment.first_name,
        'email': appointment.email,   
        'message': appointment.request}, follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertRedirects(response, reverse('home'))
    def test_contact_post_gives_sending_message(self):
        appointment = AppointmentFactory() 
        response = self.client.post(reverse('home'),  {'name' : appointment.first_name,
        'email': appointment.email,   
        'message': appointment.request}, follow=True)
        self.assertEqual(response.status_code, 200)
        messages = list(response.context['messages'])
        self.assertEqual(len(messages), 1)
        self.assertEqual(str(messages[0]), "Thanks " + appointment.first_name + " for your email!")
    def test_contact_post_sends_email(self): 
        appointment = AppointmentFactory()
        response = self.client.post(reverse('home'),  {'name' : appointment.first_name,
        'email': appointment.email,   
        'message': appointment.request}, follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(mail.outbox[0].subject, appointment.first_name + ' from CarAge front page.')
        self.assertEqual(mail.outbox[0].body, appointment.request)

class AppointmentListTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        number_of_appointments = 15
        cls.appointments = AppointmentFactory.create_batch(size=number_of_appointments)
        cls.appointments[0].accepted=False
        cls.appointments[0].accepted_date=None
        test_user1 = User.objects.create_user(username='testuser1', password='okmokmokm')
        test_user1.save()

    def test_view_url_exists_at_desired_location(self):
        login = self.client.login(username='testuser1', password='okmokmokm')
        response = self.client.get('/appointments/')
        self.assertEqual(response.status_code, 200)
    def test_redirect_if_not_logged_in(self):
        response = self.client.get('/appointments/')
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, '/accounts/login/?next=/appointments/')
    def test_view_url_accessible_by_name(self):
        login = self.client.login(username='testuser1', password='okmokmokm')
        response = self.client.get(reverse('appointmentlist'))
        self.assertEqual(response.status_code, 200)
    def test_logged_in_view_uses_correct_template(self):
        login = self.client.login(username='testuser1', password='okmokmokm')
        response = self.client.get(reverse('appointmentlist'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'appointment_list.html')
    def test_appointment_acceptance_redirects(self):
        login = self.client.login(username='testuser1', password='okmokmokm')
        response = self.client.post(reverse('appointmentlist'),  {'date' : self.appointments[0].appointment_date,
        'appointment-id': self.appointments[0].id}, follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertRedirects(response, reverse('appointmentlist'))
    def test_empty_appointment_acceptance_gives_message(self):
        login = self.client.login(username='testuser1', password='okmokmokm')
        response = self.client.post(reverse('appointmentlist'),  {'date' : "",
        'appointment-id': self.appointments[0].id}, follow=True)
        self.assertEqual(response.status_code, 200)
        messages = list(response.context['messages'])
        self.assertEqual(len(messages), 1)
        self.assertEqual(str(messages[0]), "Cannot accept without an appointment date!")
    def test_empty_appointment_acceptance_redirects(self):
        login = self.client.login(username='testuser1', password='okmokmokm')
        response = self.client.post(reverse('appointmentlist'),  {'date' : "",
        'appointment-id': self.appointments[0].id}, follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertRedirects(response, reverse('appointmentlist'))
    def test_appointment_acceptance_gives_message(self): 
        login = self.client.login(username='testuser1', password='okmokmokm')
        response = self.client.post(reverse('appointmentlist'),  {'date' : self.appointments[0].appointment_date,
        'appointment-id': self.appointments[0].id}, follow=True)
        self.assertEqual(response.status_code, 200)
        messages = list(response.context['messages'])
        self.assertEqual(len(messages), 1)
        self.assertEqual(str(messages[0]), "You accepted the appointment of " + self.appointments[0].first_name + ". Email was sent.")
    def test_appointment_acceptance_sends_email(self): 
        login = self.client.login(username='testuser1', password='okmokmokm')
        response = self.client.post(reverse('appointmentlist'),  {'date' : self.appointments[0].appointment_date,
        'appointment-id': self.appointments[0].id}, follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(mail.outbox[0].subject,'About your time request')


class AppointmentDeleteTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        number_of_appointments = 5
        cls.appointments = AppointmentFactory.create_batch(size=number_of_appointments)
        test_user1 = User.objects.create_user(username='testuser1', password='okmokmokm')
        test_user1.save()

    def test_view_url_accessible_by_name(self):
        login = self.client.login(username='testuser1', password='okmokmokm')
        response = self.client.get(reverse('appointment-delete', kwargs={'pk': self.appointments[0].id} ), follow=True)
        self.assertEqual(response.status_code, 200)     
    def test_view_url_not_accessible_for_unlogged(self):
        response = self.client.get(reverse('appointment-delete', kwargs={'pk': self.appointments[0].id} ),follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, reverse('account_login'))         
    def test_view_url_exists_at_desired_location(self):
        login = self.client.login(username='testuser1', password='okmokmokm')
        response = self.client.get('/appointment-delete/' + str(self.appointments[0].pk) + '/')
        self.assertEqual(response.status_code, 302)
    def test_deleteview_removes(self):
        login = self.client.login(username='testuser1', password='okmokmokm')
        response = self.client.get(reverse('appointment-delete', kwargs={'pk': self.appointments[0].id} ),follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertRedirects(response, reverse('appointmentlist'))

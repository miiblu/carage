from datetime import datetime, timezone#timedelta, 
import factory
import factory.fuzzy


from carage.customers.models import Appointment

class AppointmentFactory(factory.django.DjangoModelFactory):
    first_name = factory.fuzzy.FuzzyText()
    last_name = factory.fuzzy.FuzzyText()
    email = factory.Sequence(lambda n: 'person{}@example.com'.format(n))
    phone = factory.fuzzy.FuzzyText()
    request = factory.Faker('paragraph', nb_sentences=3,  variable_nb_sentences=True)
    appointment_date = factory.fuzzy.FuzzyDate(
        datetime(2020, 1, 1),
        datetime(2025, 12, 31, 20))
    sent_date = factory.fuzzy.FuzzyDate(
        datetime(2020, 1, 1),
        datetime(2025, 12, 31, 20))
    accepted = factory.Faker('pybool')
    accepted_date = factory.fuzzy.FuzzyDate(
        datetime(2020, 1, 1),
        datetime(2025, 12, 31, 20))#, tzinfo=timezone.utc))
    class Meta:
        model = Appointment
const path = require("path");

module.exports = {
  publicPath: process.env.VUE_APP_STATIC_URL,
  outputDir: path.resolve(__dirname, "../static", "customers"),
  indexPath: path.resolve(__dirname, "../templates/", "customers", "invoice.html"),
  devServer: {
    proxy: "http://127.0.0.1:8000"
  }
};


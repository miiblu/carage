from rest_framework.generics import CreateAPIView, ListAPIView#, ListCreateAPIView
#from rest_framework.authentication import SessionAuthentication
from .serializers import InvoiceSerializer
from .serializers import AppointmentSerializer, Appointment
# from django.views.decorators.csrf import csrf_exempt
# from django.utils.decorators import method_decorator

class AppointmentList(ListAPIView):
    '''ApiView for appointment list'''
    serializer_class = AppointmentSerializer
    #queryset = Appointment.objects.all()
    queryset = Appointment.objects.filter(accepted=True)



# @method_decorator(csrf_exempt, name='dispatch')
class InvoiceCreate(CreateAPIView): 
    '''ApiView for creating an invoice'''
    #authentication_classes = [SessionAuthentication]  #added
    serializer_class = InvoiceSerializer

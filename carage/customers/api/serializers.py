from rest_framework import serializers
#from carage.users.models import User
from carage.customers.models import Appointment, Invoice, ItemLine

class AppointmentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Appointment
        #fields = '__all__'
        fields = ["id","accepted", "first_name", "last_name" ,"appointment_date" ,"email", "numberOfInvoices"]


class ItemLineSerializer(serializers.ModelSerializer):
    class Meta:
        model = ItemLine
        fields = ["quantity", "description", "price", "taxed"]


class InvoiceSerializer(serializers.ModelSerializer):
    items = ItemLineSerializer(many=True)#, read_only=True)

    class Meta:
        model = Invoice
        fields = ["appointment", "date", "due_date", "items"]

    def create(self, validated_data):
        print("serializers.py create")
        items = validated_data.pop("items")
        invoice = Invoice.objects.create(**validated_data)
        for item in items:
            ItemLine.objects.create(invoice=invoice, **item)
        return invoice

from django.db import models
from django.utils.text import slugify
from autoslug import AutoSlugField
from model_utils.models import TimeStampedModel



def namesSlugify(content):
    # '''Function to generate slug for Customer instances.'''
    # nslug = slugify('%s %s' % (content.firstname, content.lastname))
    # count = Customer.objects.filter(slug__startswith=nslug).count()
    # if count == 0:
    #     return nslug
    # else:
    #     return '%s-%d' % (nslug, count)
        raise NotImplementedError

# class Customer(TimeStampedModel):
#     '''Links appointments of a customer together.
#     This is for a permanently registered customers only. Avoid use for now.
#     '''
#     first_name = models.CharField("First name", max_length=255)
#     last_name = models.CharField("Last name", max_length=255)
#     slug = AutoSlugField("Names slug",slugify=namesSlugify, unique=True, 
#     always_update=False, populate_from=['firstname', 'lastname'], default="")
#     created_at = models.DateTimeField(auto_now_add=True)
#     def __str__(self):
#         return str(self.slug)


class Appointment(models.Model):
    '''Stores appointment information.
    Includes customer information, avoiding a
    customer register as such.
    '''
    first_name = models.CharField(max_length=50, blank=False)
    last_name = models.CharField(max_length=50, blank=False)
    email = models.EmailField(max_length=50, blank=False)
    phone = models.CharField(max_length=50, blank=False)
    request = models.TextField(blank=False)
    appointment_date = models.DateField(blank=True, null=True)
    sent_date = models.DateField(auto_now_add=True)
    accepted = models.BooleanField(default=False)
    accepted_date = models.DateField(auto_now_add=False, null=True, blank=True)
    # registered_customer = models.ForeignKey(
    #     to=Customer, on_delete=models.PROTECT, related_name="appointments",
    #     null=True, blank=True
    # )

    def __str__(self):
        return self.first_name+ " " + self.last_name + " (" + str(self.appointment_date) +")"

    def numberOfInvoices(self):#, *args, **kwargs):
        result = self.invoices.values().count()
        return result

    class Meta:
        ordering = ["accepted","appointment_date"]


class Invoice(models.Model):
    '''Stores invoice information or links the items within an invoice.
    customer information comes from Appointment.
    '''
    class State(models.TextChoices):
        PAID = "PAID"
        UNPAID = "UNPAID"
        CANCELLED = "CANCELLED"

    appointment = models.ForeignKey(
        to=Appointment, on_delete=models.PROTECT, related_name="invoices",
        null=True, blank=True, default=None
        )
    date = models.DateField() 
    due_date = models.DateField()
    state = models.CharField(max_length=20, choices=State.choices, default=State.UNPAID)

    class Meta:
        ordering = ["due_date"]

class ItemLine(models.Model):
    '''Stores item information for each line of an invoice.'''
    invoice = models.ForeignKey(
        to=Invoice, on_delete=models.PROTECT, related_name="items",
        null=True, blank=True, default=None
        )
    quantity = models.IntegerField()
    description = models.CharField(max_length=500)
    price = models.DecimalField(max_digits=8, decimal_places=2)
    taxed = models.BooleanField()

    class Meta:
        ordering = ["description"]


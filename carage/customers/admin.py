from django.contrib import admin
from carage.customers.models import Appointment, Invoice,ItemLine #, Customer


class AppointmentAdmin(admin.ModelAdmin):
    model=Appointment
    list_display = ('first_name', 'last_name',   'sent_date'  )
    list_filter = ('sent_date', 'accepted_date' ,'appointment_date' )
    readonly_fields=('sent_date', 'accepted_date')
    search_fields = ('first_name', 'last_name')


class ItemLineInLine(admin.TabularInline):
    model = ItemLine
    extra = 0

class InvoiceAdmin(admin.ModelAdmin):
    model=Invoice
    list_display = ('appointment', 'date',   'due_date'  )
    list_filter = ('state',)
    inlines = (
        ItemLineInLine,
        )

admin.site.register(Appointment, AppointmentAdmin)
admin.site.register(Invoice, InvoiceAdmin)

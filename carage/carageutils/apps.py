from django.apps import AppConfig


class CarageutilsConfig(AppConfig):
    name = 'carage.carageutils'

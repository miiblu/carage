from django.contrib import admin
from django.contrib.auth import admin as auth_admin
from django.contrib.auth import get_user_model

from carage.users.forms import (
    UserChangeForm,
    UserCreationForm,
)

User = get_user_model()


@admin.register(User)
class UserAdmin(auth_admin.UserAdmin):

    form = UserChangeForm
    add_form = UserCreationForm
    fieldsets = (
        ("User", {"fields": ("name",)}),
    ) + auth_admin.UserAdmin.fieldsets
    list_display = ["username", "name", "is_superuser"]
    search_fields = ["name"]


from django.contrib import admin
from carage.customers.models import Appointment #, Customer


class AppointmentAdmin(admin.ModelAdmin):
    model=Appointment
    list_filter = ('first_name', 'last_name')
    readonly_fields=('sent_date', 'accepted_date')

# Register your models here.
#admin.site.register(Appointment)

#admin.site.register(Customer)